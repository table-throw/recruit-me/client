module.exports = {
  bracketSpacing: false,
  singleQuote: true,
  trailingComma: 'all',
  "tabWidth": 2,
};
