import React, {useState, useContext, useEffect} from 'react';


import {AuthContext} from './context/AuthContext';
import Authentication from './authentication/Authentication';
import RecruiterHome from './RecruiterHome';
import AsyncStorage from '@react-native-community/async-storage';


const NavigationHome = ({navigation}) => {
  const [] = useState(null);
  // const {actions} = useContext(AuthContext);

  // useEffect(() => {
  //   AsyncStorage.getItem('jwt')
  //     .then( jwt => {
  //       AsyncStorage.getItem('userInfos')
  //         .then(userInfos=> {
  //           actions.setUserInfos(jwt, userInfos);
  //         })
  //     })
  // }, []);
  return (
    <>
      <AuthContext.Consumer>
        {({selectors}) => (
          <>
            {!selectors.isLogged() && (
              <Authentication />
            )}
            {selectors.isLogged() && (
              <RecruiterHome />
            )}
          </>
        )}
      </AuthContext.Consumer>
    </>
  );
};

export default NavigationHome;
