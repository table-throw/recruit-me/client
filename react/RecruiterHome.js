import React, {useState, useEffect} from 'react';
import {StyleSheet} from 'react-native';
import {Card, Title, Paragraph, Button, IconButton} from 'react-native-paper';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import OfferList from './offer/OfferList';
import OfferAdd from './offer/OfferAdd';
import CandidateList from './candidates/CandidateList';
import CandidateForm from './candidates/CandidateForm';

const Tab = createMaterialBottomTabNavigator();

const RecruiterHome = ({navigation}) => {
  const [] = useState(null);

  return (
    <Tab.Navigator>
      <Tab.Screen
        name="offerList"
        component={OfferList}
        options={{
          tabBarLabel: 'Listes des offres',
          tabBarIcon: ({color}) => (
            <MaterialCommunityIcons name="login" color={color} size={26} />
          ),
        }}
      />
      <Tab.Screen
        name="offerAdd"
        component={OfferAdd}
        options={{
          tabBarLabel: 'Ajouter une offre',
          tabBarIcon: ({color}) => (
            <MaterialCommunityIcons name="logout" color={color} size={26} />
          ),
        }}
      />
      <Tab.Screen
        name="CandidateList"
        component={CandidateList}
        options={{
          tabBarLabel: 'Listes des candidatures',
          tabBarIcon: ({color}) => (
            <MaterialCommunityIcons name="logout" color={color} size={26} />
          ),
        }}
      />
      {/* <Tab.Screen
        name="CandidateForm"
        component={CandidateForm}
        options={{
          tabBarLabel: 'Ajouter une candidature',
          tabBarIcon: ({color}) => (
            <MaterialCommunityIcons name="logout" color={color} size={26} />
          ),
        }}
      /> */}
    </Tab.Navigator>
  );
};

export default RecruiterHome;
