import React, { useState, useEffect, Component } from 'react';
import { StyleSheet } from 'react-native';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import Login from './Login';
import Register from './Register';

const Tab = createMaterialBottomTabNavigator();

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    // alignItems: 'center',
  },
  input: {
    color: 'blue',
    fontWeight: 'bold',
    fontSize: 30,
  },
});

const Authentication = () => {
  return (
    <>
      <Tab.Navigator>
        <Tab.Screen
          name="login"
          component={Login}
          options={{
            tabBarLabel: 'Connexion',
            tabBarIcon: ({ color }) => (
              <MaterialCommunityIcons name="login" color={color} size={26} />
            ),
          }}
        />
        <Tab.Screen
          name="register"
          component={Register}
          options={{
            tabBarLabel: 'Inscription',
            tabBarIcon: ({ color }) => (
              <MaterialCommunityIcons name="logout" color={color} size={26} />
            ),
          }}
        />
      </Tab.Navigator>
    </>
  );
};

export default Authentication;
