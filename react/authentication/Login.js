import React, {useState, useEffect, Component} from 'react';
import {StyleSheet, View} from 'react-native';
import {Button, TextInput, Card, Text} from 'react-native-paper';

import AsyncStorage from '@react-native-community/async-storage';

import { AuthContext } from '../context/AuthContext';
const styles = StyleSheet.create({
  container: {
    height: '100%',
    flexDirection: 'column',
    justifyContent: 'center',
    marginLeft: 10,
    marginRight: 10,
  },
  input: {
    marginTop: 10,
  },
  btn: {
    fontSize: 15,
    fontWeight: 'bold',
  },
});

class Login extends Component {
  constructor(props) {
    super(props);
    this.email = '';
    this.password = '';
  }

  login() {
    this.context.actions.login({
      email: this.email,
      password: this.password,
    });
  }

  render() {
    // console.log(this.props.route.params.email);
    if (this.props.route.params) {
      this.email = this.props.route.params.email;
    }
    return (
      <View style={styles.container}>
        <TextInput
          style={styles.input}
          label='Email' 
          onChangeText={text => {
            this.email = text;
          }}
          mode="outlined"
        />
        <TextInput
          style={styles.input}
          label='Mot de passe'
          onChangeText={text => {
            this.password = text;
          }}
          secureTextEntry={true}
          mode="outlined"
        />
        <Button
          style={[styles.input, styles.btn]}
          mode="contained"
          onPress={this.login.bind(this)}>
          Connexion
        </Button>
      </View>
    );
  }
}

Login.contextType = AuthContext;

export default Login;
