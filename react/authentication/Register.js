import React, {useState, useEffect, Component} from 'react';
import {StyleSheet, View, ToastAndroid} from 'react-native';
import {Button, TextInput, RadioButton, Text} from 'react-native-paper';
import AsyncStorage from '@react-native-community/async-storage';

import ApiHost from '../../api.config';
import {AuthContext} from '../context/AuthContext';

const styles = StyleSheet.create({
  container: {
    height: '100%',
    flexDirection: 'column',
    justifyContent: 'center',
    marginLeft: 10,
    marginRight: 10,
  },
  input: {
    marginTop: 10,
  },
  btn: {
    fontSize: 15,
    fontWeight: 'bold',
  },
  radioContainer: {
    flex: 1,
    flexDirection: 'row',
    width: '50%',
  },
  radioBtn: {
    width: 40,
  },
});

const Toast = ({visible, message}) => {
  if (visible) {
    ToastAndroid.showWithGravityAndOffset(
      message,
      ToastAndroid.LONG,
      ToastAndroid.BOTTOM,
      25,
      150,
    );
    return null;
  }
  return null;
};

class Register extends Component {
  static contextType = AuthContext;

  constructor(props) {
    super(props);
    this.email = '';
    this.password = '';
    this.state = {
      userRole: '',
      toastVisible: false,
    };
  }

  register() {
    // console.log(this.context.state);
    this.context.actions.signup({
      email: this.email,
      password: this.password,
      roles: this.state.userRole,
    });
  }

  render() {
    console.log('state: ', this.context.state);
    if (this.context.state.registered) {
      this.props.navigation.navigate('login', {
        // email: this.context.state.userInfos.email,
      });
    }
    return (
      <View style={styles.container}>
        <Toast visible={this.context.state.signupToast.visible} message={this.context.state.signupToast.message} />
        <TextInput
          style={styles.input}
          label='Email'
          autoCompleteType="email"
          onChangeText={text => {
            this.email = text;
          }}
          mode="outlined"
        />
        <TextInput
          style={styles.input}
          label='Mot de passe' 
          autoCompleteType="password"
          onChangeText={text => {
            this.password = text;
          }}
          secureTextEntry={true}
          mode="outlined"
        />
        <RadioButton.Group
          style={styles.radioContainer}
          onValueChange={value => this.setState({ userRole : value })}
          value={this.state.userRole}>
          <View>
            <Text>Candidat</Text>
            <RadioButton value="ROLE_APPLICANT" style={styles.radioBtn} />
          </View>
          <View>
            <Text>Recruteur</Text>
            <RadioButton value="ROLE_RECRUITER" style={styles.radioBtn} />
          </View>
        </RadioButton.Group>
        <Button
          style={[styles.input, styles.btn]}
          mode="contained"
          onPress={this.register.bind(this)}>
          Connexion
        </Button>
      </View>
    );
  }
}

export default Register;
