export default (state, action) => {
  switch (action.type) {
    case 'LOG_USER':
      return {
        ...state,
        jwt: action.payload.token,
        userInfos: action.payload.userInfos,
      };
    case 'SIGNUP_USER':
      return {
        ...state,
        userInfos: action.payload.userInfos,
        registered: action.payload.registered,
        signupToast: action.payload.signupToast,
      };
    default:
      return state;
  }
};
