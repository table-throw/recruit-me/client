import ApiHost from '../../../api.config';

export const login = ({email, password}) => {
  return fetch(ApiHost + '/authentication_token', {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      email: email,
      password: password,
    }),
  })
    .then(response => response.json())
    .then(json => {
      return json;
    })
    .catch(error => console.log('error : ', error));
};

export const getUserInfos = jwt => {
  return fetch(ApiHost + '/users/profile', {
    method: 'Get',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + jwt,
    },
  })
    .then(response => response.json())
    .then(json => {
      return json;
    })
    .catch(error => console.log('error : ', error));
};

export const signup = ({email, password, roles}) => {
  return fetch(ApiHost + '/users', {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      email: email,
      password: password,
      roles: [roles],
    }),
  })
    .then(response => response.json())
    .then(json => {
      return json;
    })
    .catch(error => console.log('error : ', error));
};
