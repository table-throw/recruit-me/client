import React, {createContext, useState, useReducer} from 'react';
import {login, signup, getUserInfos} from './actions/security';
import AsyncStorage from '@react-native-community/async-storage';
import securityReducer from './reducers/security';

export const AuthContext = createContext();

const initialState = {
  userInfos: null,
  jwt: null,
  registered: false,
  signupToast: {
    visible: false,
    message: '',
  },
};

const reducer = (state, action) => {
  state = securityReducer(state, action);
  return state;
};

export const AuthProvider = ({children}) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  const selectors = {
    getUser: () => state.userInfos,
    isLogged: () => {
      // const userinfos = await AsyncStorage.getItem('userInfos');
      // const jwt = await AsyncStorage.getItem('jwt');
      // console.log('infos: ', !!state && !!state.userInfos && !!state.jwt);
      return !!state && !!state.userInfos && !!state.jwt;
    },
    getJwt: async () => {
      return await AsyncStorage.getItem('jwt');
    },
    getUserInfos: () => AsyncStorage.getItem('userInfos'),
  };

  const actions = {
    login: async _user => {
      const data = await login(_user);
      await AsyncStorage.setItem('jwt', JSON.stringify(data.token));
      const userInfos = await getUserInfos(data.token);
      await AsyncStorage.setItem('userInfos', JSON.stringify(userInfos));
      dispatch({
        type: 'LOG_USER',
        payload: {
          token: data.token,
          userInfos: userInfos,
        },
      });
    },
    signup: async _user => {
      const data = await signup(_user);
      console.log('data ', data);
      dispatch({
        type: 'SIGNUP_USER',
        payload: {
          userInfos: data,
          registered: true,
          signupToast: {
            visible: true,
            message: 'Inscription réussie',
          },
        },
      });
      return data;
    },
    setUserInfos: (jwt, userInfos) => {
      dispatch({
        type: 'LOG_USER',
        payload: {
          token: jwt,
          userInfos: userInfos,
        },
      });
    },
  };

  return (
    <AuthContext.Provider value={{state, selectors, actions}}>
      {children}
    </AuthContext.Provider>
  );
};
