/* eslint-disable quotes */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, {Component} from 'react';
import {View, TextInput, Button, Text, StyleSheet} from 'react-native';
import ApiHost from '../../api.config';

import {AuthContext} from '../context/AuthContext';

const styles = StyleSheet.create({
  input: {
    marginLeft: 5,
    marginRight: 5,
    height: 50,
    borderColor: '#000000',
    borderWidth: 1,
    paddingLeft: 5
  },
});

export default class OfferAdd extends Component {
  static contextType = AuthContext;
  constructor(props) {
    super(props);

    this.state = {
      name: "",
      descriptionCompany: "",
      description: "",
      typeContract: "",
      workplace: "",
    };

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();

    const test = {
      name: this.state.name,
      descriptionCompany: this.state.descriptionCompany,
      description: this.state.description,
      typeContract: this.state.typeContract,
      workplace: this.state.workplace,
    };

    fetch(ApiHost + '/offers', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.context.state.jwt,
      },
      // body: JSON.stringify({
      //   name: this.state.name,
      //   descriptionCompany: this.state.descriptionCompany,
      //   description: this.state.description,
      //   typeContract: this.state.typeContract,
      //   workplace: this.state.workplace,
      // })
      body: JSON.stringify(test),
    })
    .then((json) => {
      console.log(json);
      // Redirection vers Ajout Candidat
    })
    .catch((error) => {
      console.error(error);
    });
  }

  render() {

    return (
      <View style={{flex: 1, padding: 24}}>
        <Text>Ajout d'une offre</Text>
        <View>
          <TextInput
            style={styles.input}
            value={this.state.name}
            onChangeText={text => {
              this.setState({name: text})
            }}
            placeholder="Nom de l'offre"
          />
          <TextInput
            style={styles.input}
            value={this.state.descriptionCompany}
            onChangeText={text => {
              this.setState({descriptionCompany: text})
            }}
            placeholder="Entreprise"
          />
          <TextInput
            style={styles.input}
            value={this.state.description}
            onChangeText={text => {
              this.setState({description: text})
            }}
            placeholder="Description"
          />
          <TextInput
            style={styles.input}
            value={this.state.typeContract}
            onChangeText={text => {
              this.setState({typeContract: text})
            }}
            placeholder="Type de Contrat"
          />
          <TextInput
            style={styles.input}
            value={this.state.workplace}
            onChangeText={text => {
              this.setState({workplace: text})
            }}
            placeholder="Lieu de travail"
          />
          <Button
            title="Ajouter"
            onPress={this.handleSubmit}
          />
      </View>
      </View>
    );
  }

}
