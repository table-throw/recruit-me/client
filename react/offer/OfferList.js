/* eslint-disable quotes */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, {Component} from 'react';
import {View, Text} from 'react-native';
import {List, Title, Button} from 'react-native-paper';
import ApiHost from '../../api.config';

import {AuthContext} from '../context/AuthContext';

class OfferList extends Component {
  static contextType = AuthContext;
  constructor(props) {
    super(props);

    this.state = {
      data: [],
    };

    this.getFiche = this.getFiche.bind(this);
  }

  async componentDidMount() {
    fetch(ApiHost + '/offers', {
      method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + this.context.state.jwt,
        },
    })
      .then((response) => response.json())
      .then((json) => {
        this.setState({ data: json["hydra:member"]});
      })
      .catch((error) => console.error(error));
  }

  getFiche(id){
    console.log(id);
    console.log('redirect to fiche not functionnal');
    // this.props.navigate.navigation('OfferFiche', {offerId: id});
  }

  render() {
    return (
      <View style={{flex: 1, padding: 24}}>
        <Title>Liste des Offres</Title>
          {
            // this.state.data.map((offre) => {
            //     return (
            //       <List.Item
            //         title={offre.name + ", " + offre.descriptionCompany + ", " + offre.typeContract}
            //         description={offre.description + ", " + offre.descriptionCompany}
            //         key={offre.id}
            //         onPress={() => this.getFiche(offre.id)}
            //       />
            //       );
            //   })
          }
        <Button icon="briefcase-plus" mode="contained" onPress={() => this.props.navigation.navigate('offerAdd')}>
          Ajouter une offre
        </Button>
      </View>
    );
  }
}

export default OfferList;
