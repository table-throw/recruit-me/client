/* eslint-disable quotes */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {List, Title, Button} from 'react-native-paper';
import ApiHost from '../../api.config';

const styles = StyleSheet.create({
});

export default class OfferFiche extends Component {
  constructor(props) {
    super(props);

    this.state = {
        data: [],
    };
  }

  componentDidMount() {
    fetch(ApiHost + '/offers/1')
      .then((response) => response.json())
      .then((json) => {
          console.log(json);
        this.setState({ data: json});
      })
      .catch((error) => console.error(error));
  }

  render() {

    return (
        <View style={{flex: 1, padding: 24}}>
            <Title>{this.state.data.name}</Title>
            <Text>Entreprise: {this.state.data.descriptionCompany}</Text>
            <Text>Détail de l'offre: {this.state.data.description}</Text>
            <Text>Lieux de travail: {this.state.data.workplace}</Text>
            <Text>Type de contrat: {this.state.data.typeContract}</Text>
        </View>
    );
  }

}
