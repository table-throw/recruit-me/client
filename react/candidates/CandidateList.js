import React, { useState, useEffect } from 'react';
import { StyleSheet, SafeAreaView } from 'react-native';
import { Text, List } from 'react-native-paper';
import { apiHost } from '../../api.config.js'
const CandidateList = ({ navigation }) => {

  fetch(apiHost + '/applications', {
    method: 'GET',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    }
  })
    .then(response => response.json())
    .then(json => {
      data = json
    })
    .catch(error => console.log('error : ', error));

  let data = [
    {
      id: 1,
      title: "Test",
      description: "Description",
    },
    {
      id: 2,
      title: "Test2",
      description: "Description2",
    }
  ]

  function fillForm(item) {
    navigation.navigate('CandidateForm', { offerId: item.id })
  }

  return (
    <>
      <SafeAreaView style={styles.global}>
        {data.map((item) =>
          <List.Item
            key={item.id}
            title={item.title}
            description={item.description}
            left={props => <List.Icon {...props} icon="file" />}
            onPress={() => fillForm(item)}
          />
        )}

      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  global: {
    marginHorizontal: 10,
    marginVertical: 10,
  },
  title: {
    fontSize: 20,
  }
});

export default CandidateList;
