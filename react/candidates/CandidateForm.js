import React, { useState, useEffect } from 'react';
import { StyleSheet, SafeAreaView, View, Image } from 'react-native';
import { TextInput, RadioButton, Text, Button } from 'react-native-paper';
import { Picker } from '@react-native-community/picker';
import { ScrollView } from 'react-native-gesture-handler';
import ImagePicker from 'react-native-image-picker';
import { apiHost } from '../../api.config.js';

const CandidateForm = ({ route, navigation }) => {
  const [photo, setPhoto] = useState(null);
  const [firstName, setFirstName] = useState("");
  const [name, setName] = useState("");
  const [gender, setGender] = useState("male");
  const [email, setEmail] = useState("");
  const [age, setAge] = useState("");
  const [address, setAddress] = useState("");
  const [motivationField, setMotivationField] = useState("");
  const [salaryClaim, setSalaryClaim] = useState("");

  const { offerId } = route.params;

  const options = {
    title: 'Choisir une image depuis...',
    storageOptions: {
      skipBackup: true,
      path: 'images',
    },
  };

  function sendForm() {
    return fetch(apiHost + '/applications', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        gender: gender,
        firstName: firstName,
        name: name,
        photo: photo,
        email: email,
        age: age,
        address: address,
        motivationField: motivationField,
        salaryClaim: salaryClaim,
        cv: ""
      }),
    })
      .then(response => response.json())
      .then(json => {
        navigation.navigate('CandidateList')
        return json;
      })
      .catch(error => console.log('error : ', error));
  }

  function showImagePicker() {
    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else {
        const source = { uri: response.uri };

        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };

        setPhoto(source);
      }
    });
  }

  return (
    <>
      <ScrollView style={styles.global}>
        <Button icon="camera" mode="contained" onPress={() => showImagePicker()}>
          Photo
        </Button>

        <TextInput
          label='Nom'
          mode='outlined'
          value={name}
          onChangeText={text => setName(text)}
        />
        <TextInput
          label='Prénom'
          mode='outlined'
          value={firstName}
          onChangeText={text => setFirstName(text)}
        />

        <Text style={styles.mTop}>Sexe</Text>
        <Picker
          selectedValue={gender}
          style={{ height: 50, width: 200 }}
          onValueChange={(itemValue, itemIndex) =>
            setGender(itemValue)
          }>
          <Picker.Item label="Homme" value="male" />
          <Picker.Item label="Femme" value="female" />
        </Picker>

        <TextInput
          label='Mail'
          mode='outlined'
          value={email}
          onChangeText={text => setEmail(text)}
        />

        <TextInput
          label='Âge'
          mode='outlined'
          value={age}
          onChangeText={text => setAge(text)}
        />

        <TextInput
          label='Adresse'
          mode='outlined'
          value={address}
          onChangeText={text => setAddress(text)}
        />


        <TextInput
          label='Motivation'
          multiline
          numberOfLines={4}
          mode='outlined'
          value={motivationField}
          onChangeText={text => setMotivationField(text)}
        />

        <TextInput
          label='Prétentions salariales'
          mode='outlined'
          value={salaryClaim}
          onChangeText={text => setSalaryClaim(text)}
        />

        <Button style={styles.mTop} icon="check" mode="contained" onPress={() => sendForm()}>
          Envoyer
        </Button>

      </ScrollView>
    </>
  );
};

const styles = StyleSheet.create({
  image: {
    backgroundColor: '#000'
  },
  global: {
    marginHorizontal: 10,
    marginVertical: 10,
  },
  title: {
    fontSize: 20,
  },
  mTop: {
    marginTop: 10
  }
});

export default CandidateForm;
