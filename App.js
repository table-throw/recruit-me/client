/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import { SafeAreaView, StatusBar, View } from 'react-native';
import { Provider as PaperProvider } from 'react-native-paper';
import { NavigationContainer } from '@react-navigation/native';

import {AuthProvider} from './react/context/AuthContext';
import NavigationHome from './react/NavigationHome';

const App: () => React$Node = () => {
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={{ flex: 1 }}>
        <PaperProvider>
          <AuthProvider>
            <NavigationContainer>
              <NavigationHome />
            </NavigationContainer>
          </AuthProvider>
        </PaperProvider>
      </SafeAreaView>
    </>
  );
};

export default App;
