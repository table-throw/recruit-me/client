const apiHost = __DEV__ ? 'https://localhost:8443': 'https://safe-sea-98130.herokuapp.com';

export default apiHost;
